![](https://images.microbadger.com/badges/image/scramblesim/godot-desktop-export.svg)
![](https://images.microbadger.com/badges/version/scramblesim/godot-desktop-export.svg)
![](https://img.shields.io/badge/license-MIT-blue)

Smallest Docker image for exporting Godot games to Linux, macOS, and Windows.

Hosted on [Docker Hub](https://hub.docker.com/r/scramblesim/godot-desktop-export/)

## Usage
```bash
# Running Godot headless in docker
docker run scramblesim/godot-desktop-export:3.2 godot --version

# Exporting with Godot
docker run scramblesim/godot-desktop-export godot -v --export "Linux" ../path/to/build/in/ExportMe.x64

# Building this image yourself
sudo docker build -t <image-name> --build-arg GODOT_VERSION=3.2 .
```

## Godot versions
* 3.0.6
* 3.1
* 3.2

## Resources
Heavily inspired by https://github.com/aBARICHELLO/godot-ci.

