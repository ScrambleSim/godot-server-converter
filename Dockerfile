# This downloads the server version of Godot once.
# It can be used to replace the Godot binary of exports,
# which is not fit for server environments.

FROM alpine

ARG GODOT_VERSION

RUN wget -O Godot_server.zip https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_server.64.zip \
&& unzip Godot_server.zip \
&& rm Godot_server.zip \
&& mv Godot_v${GODOT_VERSION}-stable_linux_server.64 Godot_server.x64

